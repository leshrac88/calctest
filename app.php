<?php
/**
 * Требования к проекту
 *
 * Необходимо реализовать web-приложение, которое позволяет пользователю делать
 * простые операции над числами (сложение, вычитание, умножение, деление, возведение в степень).
 *
 * Приложение должно быть удобным (user friendly) и отказоустойчивым.
 */

class Math {

    /**
     * Returns sum
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    public function sum($a, $b) {
        return $a + $b;
    }

    /**
     * Returns sub
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    public function sub($a, $b) {
        return $a - $b;
    }

    /**
     * Returns mult
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    public function mult($a, $b) {
        return $a * $b;
    }

    /**
     * Returns div
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    public function div($a, $b) {
        return $a / $b;
    }

    /**
     * Returns pow
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    public function pow($a, $b) {
        return pow($a, $b);
    }
}


$a = isset($_POST['a']) ? $_POST['a'] : '0';
$b = isset($_POST['b']) ? $_POST['b'] : '0';
$operation = isset($_POST['operation']) ? $_POST['operation'] : 'sum';

$math = new Math();
$result = $math->$operation($a, $b);
?>
<html>
<head>
    <title>Онлайн калькулятор</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
    <div align="center">
        <br><br><br><br><br><br><br><br><br>
        <form action="app.php" method="post">
            <input type="text" name="a" value="<?php echo $a;?>">
            <select name="operation" value="<?php echo $operation;?>">
                <option value="sum">+</option>
                <option value="sub">-</option>
                <option value="mult">*</option>
                <option value="div">/</option>
                <option value="pow">^</option>
            </select>
            <input type="text" name="b" value="<?php echo $b;?>">
            <input type="submit" value="=">
            <?php echo $result;?>
        </form>
    </div>
</body>
</html>