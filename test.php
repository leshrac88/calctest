<?php
require "vendor/autoload.php";
include_once("app.php");
//require_once("PHPUnit\Framework\TestCase.php");

Class MathTest extends PHPUnit_Framework_TestCase
{
	private $obj;

	public function setUp()
	{
		$this->obj = new Math();
	}

	public function tearDown()
	{
		$this->obj = NULL;
	}

	public function testSum()
	{
		$this->assertEquals(10, $this->obj->sum(8, 2));
		$this->assertEquals(6, $this->obj->sum(-2, 8));
		$this->assertEquals(8, $this->obj->sum(8, 0));
		$this->assertEquals(8, $this->obj->sum("", 8));
		$this->assertEquals(8, $this->obj->sum(8, NULL));
		$this->assertEquals(-6, $this->obj->sum("2", "-8"));
		$this->assertEquals(7, $this->obj->sum("3fg", "4ghhd"));
		$this->assertEquals(6, $this->obj->sum("6fg", "gh9hd"));
		$this->assertEquals(1.93, $this->obj->sum("0.5", "1.43"));
	}

	public function testSub()
	{
		$this->assertEquals(6, $this->obj->sub(8, 2));
		$this->assertEquals(-10, $this->obj->sub(-2, 8));
		$this->assertEquals(8, $this->obj->sub(8, 0));
		$this->assertEquals(-8, $this->obj->sub("", 8));
		$this->assertEquals(8, $this->obj->sub(8, NULL));
		$this->assertEquals(10, $this->obj->sub("2", "-8"));
		$this->assertEquals(-1, $this->obj->sub("3fg", "4ghhd"));
		$this->assertEquals(6, $this->obj->sub("6fg", "gh9hd"));
		$this->assertEquals(-0.93, $this->obj->sub("0.5", "1.43"));
	}

	public function testMult()
	{
		$this->assertEquals(16, $this->obj->mult(8, 2));
		$this->assertEquals(-16, $this->obj->mult(-2, 8));
		$this->assertEquals(0, $this->obj->mult(8, 0));
		$this->assertEquals(0, $this->obj->mult("", 8));
		$this->assertEquals(0, $this->obj->mult(8, NULL));
		$this->assertEquals(-16, $this->obj->mult("2", "-8"));
		$this->assertEquals(12, $this->obj->mult("3fg", "4ghhd"));
		$this->assertEquals(0, $this->obj->mult("6fg", "gh9hd"));
		$this->assertEquals(0.715, $this->obj->mult("0.5", "1.43"));
	}

	public function testDiv()
	{
		$this->assertEquals(4, $this->obj->div(8, 2));
		$this->assertEquals(-0.25, $this->obj->div(-2, 8));
		$this->assertEquals(0, $this->obj->div(0, 8));
		$this->assertEquals(0, $this->obj->div("", 8));
		$this->assertEquals(0, $this->obj->div(NULL, 8));
		$this->assertEquals(-0.25, $this->obj->div("2", "-8"));
		$this->assertEquals(0.75, $this->obj->div("3fg", "4ghhd"));
		$this->assertEquals(0, $this->obj->div("fg6", "9gh9hd"));
		$this->assertEquals(2.86, $this->obj->div("1.43", "0.5"));
	}

	// /**
	// * @expectedException 'Exeption_name'
	// */
	// public function testDiv2()
	// {
	// 	$this->assertEquals(0, $this->obj->div(8, 0));
	// 	$this->assertEquals(0, $this->obj->div(8, NULL));
	//  $this->assertEquals(0, $this->obj->div(8, ""));
	//  $this->assertEquals(0, $this->obj->div(8, "ghb7"));
	// }

	public function testPow()
	{
		$this->assertEquals(64, $this->obj->pow(8, 2));
		$this->assertEquals(256, $this->obj->pow(-2, 8));
		$this->assertEquals(0.0625, $this->obj->pow(4, -2));
		$this->assertEquals(1, $this->obj->pow(8, 0));
		$this->assertEquals(1, $this->obj->pow(8, ""));
		$this->assertEquals(1, $this->obj->pow(8, NULL));
		$this->assertEquals(0, $this->obj->pow(0, 8));
		$this->assertEquals(0, $this->obj->pow("", 8));
		$this->assertEquals(0, $this->obj->pow(NULL, 8));
		$this->assertEquals(256, $this->obj->pow("2", "8"));
		$this->assertEquals(81, $this->obj->pow("3fg", "4ghhd"));
		$this->assertEquals(1, $this->obj->pow("6fg", "gh9hd"));
		$this->assertEquals(0.37113089265726, $this->obj->pow("0.5", "1.43"));
	}
}

?>